var _hoten = /^([A-Z\s*]{1,})$/;
var _Email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
var _sdt = /^(0)([3||9]{1,})([0-9]{8})$/;
var _diachi = /^([a-z||A-Z||0-9||\s*]{1,})$/;
var _mk = /^([a-z||A-Z||0-9]{1,})$/;

function dangky() {
    if (!hoten.value || !dc_email.value || !sdt.value || !diachi.value || !mk.value || !xn_mk.value) {
        alert("Bạn chưa điền đầy đủ thông tin");
        return false;
    }
    if (!_hoten.test(hoten.value)) {
        alert(" Họ tên phải viết hoa")
        return false;
    }
    if (!_Email.test(dc_email.value)) {
        alert(" Địa chỉ email chưa đúng cú pháp")
        return false;
    }
    if (!_sdt.test(sdt.value)) {
        alert("Số điện thoại chưa đúng cú pháp ")
        return false;
    }
    if (!_diachi.test(diachi.value)) {
        alert("Địa chỉ chưa đúng cú pháp ")
        return false;
    }
    if (!_mk.test(mk.value)) {
        alert("Mật khẩu chưa đúng cú pháp ")
        return false;
    }
    if (mk.value != xn_mk.value) {
        alert("Xác nhận mật khẩu không khớp với mật khẩu ")
        return false;
    } else {
        alert("Bạn đã đăng ký thành công");
        $('#modelId1').modal('hide')
        return true;
    }
}
function dangnhap(){
    if(email_dn.value==""||mk_dn.value==""){
        alert("Bạn chưa điền đầy đủ thông tin")
        return false
    }
    if(!_Email.test(email_dn.value)){
        alert("Email chưa đúng cú pháp");
        return false;
    }
    if(!_mk.test(mk_dn.value)){
        alert("Mật khẩu chưa đúng cú pháp");
        $('#modelId1').modal('hide')
        return false;

    }
    else{
        alert("Bjan đã đăng ký thành công")
        return true;
    }
}